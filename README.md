# README #

SBGN Extension - Read and write colors and annotations in a SBGN-ML file with a simple example.

### Description ###

This implementation aims to provide a global vision on how to read and store colour information and annotation in SBGN-ML. 

* Colors follow the notation of the SBML render extension: [SBML render](http://sbml.org/Community/Wiki/SBML_Level_3_Proposals/Rendering)
* Annotations follow the SBML annotations:

```
#!XML
    <annotation>
      <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:bqmodel="http://biomodels.net/model-qualifiers/" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/">
        <rdf:Description rdf:about="#glyph1">
          <bqbiol:is>
            <rdf:Bag>
              <rdf:li rdf:resource="http://identifiers.org/uniprot/P00561" />
            </rdf:Bag>
          </bqbiol:is>
          <bqmodel:isDescribedBy>
            <rdf:Bag>
              <rdf:li rdf:resource="http://identifiers.org/pubmed/21988831" />
            </rdf:Bag>
          </bqmodel:isDescribedBy>
        </rdf:Description>
      </rdf:RDF>
    </annotation> 

```

### Usage example ###
The code takes two arguments: one .sbgn file to read, one .sbgn file to be written.
```
javac Application.java

java Application glycolysis.sbgn output.sbgn
```


### Contact ###
* For feedbacks: [https://bitbucket.org/vtoure/sbgn-extension/issues](https://bitbucket.org/vtoure/sbgn-extension/issues)
* The SBGN team [webpage](http://sbgn.github.io/sbgn/) - [SBGN discuss](https://groups.google.com/forum/#!forum/sbgn-discuss)