# generated with VANTED V2.6.3 at Wed Jan 25 15:48:12 CET 2017
graph [
  graphbackgroundcolor "#ffffff"
  sbgn [
    role "PROCESSDESCRIPTION"
  ]
  sbml [
    model_annotation ""
    model_meta_id "_000001"
    model_name "Elowitz2000 - Repressilator"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "BIOMD0000000012"
    sbml_level "2"
    sbml_meta_id "_153818"
    sbml_version "3"
  ]
  sbml_assignment_rule_1 [
    sbml_assignment_rule_1_assignmnet_variable "t_ave"
    sbml_assignment_rule_1_function "tau_mRNA/log(2)"
    sbml_assignment_rule_1_meta_id "metaid_0500035"
  ]
  sbml_assignment_rule_2 [
    sbml_assignment_rule_2_assignmnet_variable "beta"
    sbml_assignment_rule_2_function "tau_mRNA/tau_prot"
    sbml_assignment_rule_2_meta_id "metaid_0240045"
  ]
  sbml_assignment_rule_3 [
    sbml_assignment_rule_3_assignmnet_variable "k_tl"
    sbml_assignment_rule_3_function "eff/t_ave"
    sbml_assignment_rule_3_meta_id "metaid_0400235"
  ]
  sbml_assignment_rule_4 [
    sbml_assignment_rule_4_assignmnet_variable "a_tr"
    sbml_assignment_rule_4_function "(ps_a-ps_0)*60"
    sbml_assignment_rule_4_meta_id "metaid_1000237"
  ]
  sbml_assignment_rule_5 [
    sbml_assignment_rule_5_assignmnet_variable "a0_tr"
    sbml_assignment_rule_5_function "ps_0*60"
    sbml_assignment_rule_5_meta_id "metaid_0100236"
  ]
  sbml_assignment_rule_6 [
    sbml_assignment_rule_6_assignmnet_variable "kd_prot"
    sbml_assignment_rule_6_function "log(2)/tau_prot"
    sbml_assignment_rule_6_meta_id "metaid_0010335"
  ]
  sbml_assignment_rule_7 [
    sbml_assignment_rule_7_assignmnet_variable "kd_mRNA"
    sbml_assignment_rule_7_function "log(2)/tau_mRNA"
    sbml_assignment_rule_7_meta_id "metaid_0020435"
  ]
  sbml_assignment_rule_8 [
    sbml_assignment_rule_8_assignmnet_variable "alpha"
    sbml_assignment_rule_8_function "a_tr*eff*tau_prot/(log(2)*KM)"
    sbml_assignment_rule_8_meta_id "metaid_0230035"
  ]
  sbml_assignment_rule_9 [
    sbml_assignment_rule_9_assignmnet_variable "alpha0"
    sbml_assignment_rule_9_function "a0_tr*eff*tau_prot/(log(2)*KM)"
    sbml_assignment_rule_9_meta_id "metaid_0240035"
  ]
  sbml_compartment_cell [
    sbml_compartment_cell_annotation ""
    sbml_compartment_cell_id "cell"
    sbml_compartment_cell_meta_id "_000002"
    sbml_compartment_cell_non_rdf_annotation ""
    sbml_compartment_cell_sboterm "SBO:0000290"
    sbml_compartment_cell_size "1.0"
  ]
  sbml_parameter_1 [
    sbml_parameter_1_constant "false"
    sbml_parameter_1_id "beta"
    sbml_parameter_1_meta_id "metaid_0000022"
    sbml_parameter_1_name "beta"
    sbml_parameter_1_notes ""
    sbml_parameter_1_value 0.2
  ]
  sbml_parameter_10 [
    sbml_parameter_10_constant "false"
    sbml_parameter_10_id "kd_mRNA"
    sbml_parameter_10_meta_id "metaid_0000132"
    sbml_parameter_10_name "kd_mRNA"
    sbml_parameter_10_notes ""
    sbml_parameter_10_sboterm "SBO:0000356"
  ]
  sbml_parameter_11 [
    sbml_parameter_11_constant "false"
    sbml_parameter_11_id "kd_prot"
    sbml_parameter_11_meta_id "metaid_0000133"
    sbml_parameter_11_name "kd_prot"
    sbml_parameter_11_notes ""
    sbml_parameter_11_sboterm "SBO:0000356"
  ]
  sbml_parameter_12 [
    sbml_parameter_12_constant "false"
    sbml_parameter_12_id "k_tl"
    sbml_parameter_12_meta_id "metaid_0000233"
    sbml_parameter_12_name "k_tl"
    sbml_parameter_12_notes ""
    sbml_parameter_12_sboterm "SBO:0000016"
  ]
  sbml_parameter_13 [
    sbml_parameter_13_constant "false"
    sbml_parameter_13_id "a_tr"
    sbml_parameter_13_meta_id "metaid_0900235"
    sbml_parameter_13_name "a_tr"
    sbml_parameter_13_notes ""
    sbml_parameter_13_sboterm "SBO:0000186"
  ]
  sbml_parameter_14 [
    sbml_parameter_14_id "ps_a"
    sbml_parameter_14_meta_id "metaid_0800235"
    sbml_parameter_14_name "tps_active"
    sbml_parameter_14_notes ""
    sbml_parameter_14_sboterm "SBO:0000186"
    sbml_parameter_14_value 0.5
  ]
  sbml_parameter_15 [
    sbml_parameter_15_id "ps_0"
    sbml_parameter_15_meta_id "metaid_0500235"
    sbml_parameter_15_name "tps_repr"
    sbml_parameter_15_notes ""
    sbml_parameter_15_sboterm "SBO:0000485"
    sbml_parameter_15_value 5.0E-4
  ]
  sbml_parameter_16 [
    sbml_parameter_16_constant "false"
    sbml_parameter_16_id "a0_tr"
    sbml_parameter_16_meta_id "metaid_0000234"
    sbml_parameter_16_name "a0_tr"
    sbml_parameter_16_notes ""
    sbml_parameter_16_sboterm "SBO:0000485"
  ]
  sbml_parameter_2 [
    sbml_parameter_2_constant "false"
    sbml_parameter_2_id "alpha0"
    sbml_parameter_2_meta_id "metaid_0000023"
    sbml_parameter_2_name "alpha0"
    sbml_parameter_2_notes ""
    sbml_parameter_2_sboterm "SBO:0000485"
    sbml_parameter_2_value 0.2164
  ]
  sbml_parameter_3 [
    sbml_parameter_3_constant "false"
    sbml_parameter_3_id "alpha"
    sbml_parameter_3_meta_id "metaid_0000024"
    sbml_parameter_3_name "alpha"
    sbml_parameter_3_notes ""
    sbml_parameter_3_sboterm "SBO:0000186"
    sbml_parameter_3_value 216.404
  ]
  sbml_parameter_4 [
    sbml_parameter_4_id "eff"
    sbml_parameter_4_meta_id "metaid_0000025"
    sbml_parameter_4_name "translation efficiency"
    sbml_parameter_4_notes ""
    sbml_parameter_4_value 20.0
  ]
  sbml_parameter_5 [
    sbml_parameter_5_id "n"
    sbml_parameter_5_meta_id "metaid_0000026"
    sbml_parameter_5_name "n"
    sbml_parameter_5_notes ""
    sbml_parameter_5_sboterm "SBO:0000190"
    sbml_parameter_5_value 2.0
  ]
  sbml_parameter_6 [
    sbml_parameter_6_id "KM"
    sbml_parameter_6_meta_id "metaid_0000027"
    sbml_parameter_6_name "KM"
    sbml_parameter_6_notes ""
    sbml_parameter_6_sboterm "SBO:0000288"
    sbml_parameter_6_value 40.0
  ]
  sbml_parameter_7 [
    sbml_parameter_7_id "tau_mRNA"
    sbml_parameter_7_meta_id "metaid_0000028"
    sbml_parameter_7_name "mRNA half life"
    sbml_parameter_7_sboterm "SBO:0000332"
    sbml_parameter_7_value 2.0
  ]
  sbml_parameter_8 [
    sbml_parameter_8_id "tau_prot"
    sbml_parameter_8_meta_id "metaid_0000128"
    sbml_parameter_8_name "protein half life"
    sbml_parameter_8_sboterm "SBO:0000332"
    sbml_parameter_8_value 10.0
  ]
  sbml_parameter_9 [
    sbml_parameter_9_constant "false"
    sbml_parameter_9_id "t_ave"
    sbml_parameter_9_meta_id "metaid_0000032"
    sbml_parameter_9_name "average mRNA life time"
    sbml_parameter_9_sboterm "SBO:0000348"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "volume"
    sbml_unit_definition_1_meta_id "metaid_0000029"
    sbml_unit_definition_1_name "cubic microns"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^-15 * Litre)^1.0"
    sbml_unit_definition_1_sub_unit_1_meta_id "_420934"
    sbml_unit_definition_1unit "(1.0 * 10^-15 * Litre)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "substance"
    sbml_unit_definition_2_meta_id "metaid_1000000"
    sbml_unit_definition_2_name "item"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * item)^1.0"
    sbml_unit_definition_2_sub_unit_1_meta_id "_420947"
    sbml_unit_definition_2unit "(1.0 * 10^0 * item)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "time"
    sbml_unit_definition_3_meta_id "metaid_0000030"
    sbml_unit_definition_3_name "minute"
    sbml_unit_definition_3_sub_unit_1_ "(60.0 * 10^0 * second)^1.0"
    sbml_unit_definition_3_sub_unit_1_meta_id "_420960"
    sbml_unit_definition_3unit "(60.0 * 10^0 * second)^1.0"
  ]
  directed 1
  node [
    id 1
    zlevel -1

    cluster [
      cluster "cell"
    ]
    graphics [
      x 434.36674922759846
      y 93.41741555168863
      w 108.0
      h 60.0
      fill "#CCFFCC"
      outline "#000000"
      frameThickness 2.0
      gradient 0.3
      opacity 1.0
      rounding 15.0
      type "rectangle"
    ]
    label "LacI protein"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 18
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "PX"
      type "text"
    ]
    sbgn [
      glyphid "glyph2"
      role "MACROMOLECULE"
    ]
    sbml [
      _sboterm "SBO:0000252"
      boundary_condition "false"
      compartment "cell"
      has_only_substance_units "true"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "PX"
      species_meta_id "PX"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "cell"
    ]
    graphics [
      x 547.5725745828834
      y 326.3027136243913
      w 108.0
      h 60.0
      fill "#CCFFCC"
      outline "#000000"
      frameThickness 2.0
      gradient 0.3
      opacity 1.0
      rounding 15.0
      type "rectangle"
    ]
    label "TetR protein"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 18
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "PY"
      type "text"
    ]
    sbgn [
      glyphid "glyph6"
      role "MACROMOLECULE"
    ]
    sbml [
      _sboterm "SBO:0000252"
      boundary_condition "false"
      compartment "cell"
      has_only_substance_units "true"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "PY"
      species_meta_id "PY"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "cell"
    ]
    graphics [
      x 308.6754827510962
      y 325.3027136243913
      w 108.0
      h 60.0
      fill "#CCFFCC"
      outline "#000000"
      frameThickness 2.0
      gradient 0.3
      opacity 1.0
      rounding 15.0
      type "rectangle"
    ]
    label "cI protein"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 18
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "PZ"
      type "text"
    ]
    sbgn [
      glyphid "glyph3"
      role "MACROMOLECULE"
    ]
    sbml [
      _sboterm "SBO:0000252"
      boundary_condition "false"
      compartment "cell"
      has_only_substance_units "true"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "PZ"
      species_meta_id "PZ"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "cell"
    ]
    graphics [
      x 209.59643729979325
      y 194.7773396712799
      w 108.0
      h 60.0
      fill "#CCFFCC"
      outline "#000000"
      frameThickness 2.0
      gradient 0.3
      opacity 1.0
      rounding 15.0
      type "rectangle"
    ]
    label "LacI mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 18
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "X"
      type "text"
    ]
    sbgn [
      glyphid "glyph7"
      role "MACROMOLECULE"
    ]
    sbml [
      _sboterm "SBO:0000250"
      boundary_condition "false"
      compartment "cell"
      has_only_substance_units "true"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "X"
      species_meta_id "_905769"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "cell"
    ]
    graphics [
      x 673.6917274997966
      y 176.78687893204506
      w 108.0
      h 60.0
      fill "#CCFFCC"
      outline "#000000"
      frameThickness 2.0
      gradient 0.3
      opacity 1.0
      rounding 15.0
      type "rectangle"
    ]
    label "TetR mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 18
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Y"
      type "text"
    ]
    sbgn [
      glyphid "glyph5"
      role "MACROMOLECULE"
    ]
    sbml [
      _sboterm "SBO:0000250"
      boundary_condition "false"
      compartment "cell"
      has_only_substance_units "true"
      initial_amount 20.0
      sbmlRole "species"
      species_annotation ""
      species_id "Y"
      species_meta_id "_905781"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "cell"
    ]
    graphics [
      x 434.36674922759846
      y 450.81647768910807
      w 108.0
      h 60.0
      fill "#CCFFCC"
      outline "#000000"
      frameThickness 2.0
      gradient 0.3
      opacity 1.0
      rounding 15.0
      type "rectangle"
    ]
    label "cI mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 18
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Z"
      type "text"
    ]
    sbgn [
      glyphid "glyph4"
      role "MACROMOLECULE"
    ]
    sbml [
      _sboterm "SBO:0000250"
      boundary_condition "false"
      compartment "cell"
      has_only_substance_units "true"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "Z"
      species_meta_id "_905802"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 7
    zlevel -1

    graphics [
      x 129.81678029370306
      y 195.7773396712799
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction1"
      type "text"
    ]
    oldlabel "degradation of LacI transcripts"
    sbgn [
      glyphid "glyph20"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation ""
      reaction_id "Reaction1"
      reaction_meta_id "_905823"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_mRNA*X"
      kinetic_law_meta_id "_420986"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 9
    zlevel -1

    graphics [
      x 757.5779670501672
      y 176.78687893204506
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction2"
      type "text"
    ]
    oldlabel "degradation of TetR transcripts"
    sbgn [
      glyphid "glyph23"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation ""
      reaction_id "Reaction2"
      reaction_meta_id "_905842"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_mRNA*Y"
      kinetic_law_meta_id "_421012"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 11
    zlevel -1

    graphics [
      x 434.36674922759846
      y 518.711660020592
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction3"
      type "text"
    ]
    oldlabel "degradation of CI transcripts"
    sbgn [
      glyphid "glyph22"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation ""
      reaction_id "Reaction3"
      reaction_meta_id "_905862"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_mRNA*Z"
      kinetic_law_meta_id "_421038"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 13
    zlevel -1

    graphics [
      x 209.59643729979325
      y 93.41741555168863
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction4"
      type "text"
    ]
    oldlabel "translation of LacI"
    sbgn [
      glyphid "glyph25"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000184"
      reaction_annotation ""
      reaction_id "Reaction4"
      reaction_meta_id "_905882"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "k_tl*X"
      kinetic_law_meta_id "_421076"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 16
    zlevel -1

    graphics [
      x 673.6974935857017
      y 326.3027136243913
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction5"
      type "text"
    ]
    oldlabel "translation of TetR"
    sbgn [
      glyphid "glyph24"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000184"
      reaction_annotation ""
      reaction_id "Reaction5"
      reaction_meta_id "_905903"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "k_tl*Y"
      kinetic_law_meta_id "_421112"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 19
    zlevel -1

    graphics [
      x 308.6754827510962
      y 450.81647768910807
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction6"
      type "text"
    ]
    oldlabel "translation of CI"
    sbgn [
      glyphid "glyph28"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000184"
      reaction_annotation ""
      reaction_id "Reaction6"
      reaction_meta_id "_905923"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "k_tl*Z"
      kinetic_law_meta_id "_421148"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 22
    zlevel -1

    graphics [
      x 547.5725745828834
      y 93.41741555168863
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction7"
      type "text"
    ]
    oldlabel "degradation of LacI"
    sbgn [
      glyphid "glyph31"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation ""
      reaction_id "Reaction7"
      reaction_meta_id "_905943"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_prot*PX"
      kinetic_law_meta_id "_421172"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 24
    zlevel -1

    graphics [
      x 547.5725745828834
      y 266.7367185499778
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction8"
      type "text"
    ]
    oldlabel "degradation of TetR"
    sbgn [
      glyphid "glyph21"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation ""
      reaction_id "Reaction8"
      reaction_meta_id "_905962"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_prot*PY"
      kinetic_law_meta_id "_421196"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 26
    zlevel -1

    graphics [
      x 390.623325130618
      y 325.3027136243913
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction9"
      type "text"
    ]
    oldlabel "degradation of CI"
    sbgn [
      glyphid "glyph26"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation ""
      reaction_id "Reaction9"
      reaction_meta_id "_905982"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_prot*PZ"
      kinetic_law_meta_id "_421220"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 28
    zlevel -1

    graphics [
      x 209.59643729979325
      y 325.3027136243913
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction10"
      type "text"
    ]
    oldlabel "transcription of LacI"
    sbgn [
      glyphid "glyph27"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000183"
      reaction_annotation ""
      reaction_id "Reaction10"
      reaction_meta_id "_906002"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "a0_tr+a_tr*KM^n/(KM^n+PZ^n)"
      kinetic_law_meta_id "_421256"
    ]
  ]
  node [
    id 31
    zlevel -1

    graphics [
      x 434.36674922759846
      y 176.78687893204506
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction11"
      type "text"
    ]
    oldlabel "transcription of TetR"
    sbgn [
      glyphid "glyph30"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000183"
      reaction_annotation ""
      reaction_id "Reaction11"
      reaction_meta_id "_906022"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "a0_tr+a_tr*KM^n/(KM^n+PX^n)"
      kinetic_law_meta_id "_421292"
    ]
  ]
  node [
    id 34
    zlevel -1

    graphics [
      x 547.5725745828834
      y 450.81647768910807
      w 24.0
      h 24.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "rectangle"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction12"
      type "text"
    ]
    oldlabel "transcription of CI"
    sbgn [
      glyphid "glyph29"
      role "PROCESS"
    ]
    sbml [
      _sboterm "SBO:0000183"
      reaction_annotation ""
      reaction_id "Reaction12"
      reaction_meta_id "_906042"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "a0_tr+a_tr*KM^n/(KM^n+PY^n)"
      kinetic_law_meta_id "_421329"
    ]
  ]
  node [
    id 37
    zlevel -1

    graphics [
      x 73.474164908092
      y 195.7773396712799
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction1"
      type "text"
    ]
    oldlabel "degradation of LacI transcripts"
    sbgn [
      glyphid "glyph10"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation "annotation"
      reaction_id "Reaction1"
      reaction_meta_id "_905823"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_mRNA*X"
      kinetic_law_meta_id "_420986"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 39
    zlevel -1

    graphics [
      x 815.5830703887632
      y 177.78687893204506
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction2"
      type "text"
    ]
    oldlabel "degradation of TetR transcripts"
    sbgn [
      glyphid "glyph16"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation "annotation"
      reaction_id "Reaction2"
      reaction_meta_id "_905842"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_mRNA*Y"
      kinetic_law_meta_id "_421012"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 41
    zlevel -1

    graphics [
      x 434.36674922759846
      y 582.104730938157
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction3"
      type "text"
    ]
    oldlabel "degradation of CI transcripts"
    sbgn [
      glyphid "glyph15"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation "annotation"
      reaction_id "Reaction3"
      reaction_meta_id "_905862"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_mRNA*Z"
      kinetic_law_meta_id "_421038"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 43
    zlevel -1

    graphics [
      x 73.474164908092
      y 93.41741555168863
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction4"
      type "text"
    ]
    oldlabel "translation of LacI"
    sbgn [
      glyphid "glyph11"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000184"
      reaction_annotation "annotation"
      reaction_id "Reaction4"
      reaction_meta_id "_905882"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "k_tl*X"
      kinetic_law_meta_id "_421076"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 45
    zlevel -1

    graphics [
      x 719.4335890367595
      y 326.3027136243913
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction5"
      type "text"
    ]
    oldlabel "translation of TetR"
    sbgn [
      glyphid "glyph12"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000184"
      reaction_annotation "annotation"
      reaction_id "Reaction5"
      reaction_meta_id "_905903"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "k_tl*Y"
      kinetic_law_meta_id "_421112"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 47
    zlevel -1

    graphics [
      x 308.6754827510962
      y 506.2837718068681
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction6"
      type "text"
    ]
    oldlabel "translation of CI"
    sbgn [
      glyphid "glyph8"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000184"
      reaction_annotation "annotation"
      reaction_id "Reaction6"
      reaction_meta_id "_905923"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "k_tl*Z"
      kinetic_law_meta_id "_421148"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 49
    zlevel -1

    graphics [
      x 607.875386577598
      y 93.41741555168863
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction7"
      type "text"
    ]
    oldlabel "degradation of LacI"
    sbgn [
      glyphid "glyph13"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation "annotation"
      reaction_id "Reaction7"
      reaction_meta_id "_905943"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_prot*PX"
      kinetic_law_meta_id "_421172"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 51
    zlevel -1

    graphics [
      x 547.5725745828834
      y 205.94783638419165
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction8"
      type "text"
    ]
    oldlabel "degradation of TetR"
    sbgn [
      glyphid "glyph14"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation "annotation"
      reaction_id "Reaction8"
      reaction_meta_id "_905962"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_prot*PY"
      kinetic_law_meta_id "_421196"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 53
    zlevel -1

    graphics [
      x 443.36674922759846
      y 326.3027136243913
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction9"
      type "text"
    ]
    oldlabel "degradation of CI"
    sbgn [
      glyphid "glyph9"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000179"
      reaction_annotation "annotation"
      reaction_id "Reaction9"
      reaction_meta_id "_905982"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "kd_prot*PZ"
      kinetic_law_meta_id "_421220"
      kinetic_law_sboterm "SBO:0000049"
    ]
  ]
  node [
    id 55
    zlevel -1

    graphics [
      x 209.59643729979325
      y 378.9687734811596
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction10"
      type "text"
    ]
    oldlabel "transcription of LacI"
    sbgn [
      glyphid "glyph19"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000183"
      reaction_annotation "annotation"
      reaction_id "Reaction10"
      reaction_meta_id "_906002"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "a0_tr+a_tr*KM^n/(KM^n+PZ^n)"
      kinetic_law_meta_id "_421256"
    ]
  ]
  node [
    id 57
    zlevel -1

    graphics [
      x 381.623325130618
      y 176.78687893204506
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction11"
      type "text"
    ]
    oldlabel "transcription of TetR"
    sbgn [
      glyphid "glyph17"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000183"
      reaction_annotation "annotation"
      reaction_id "Reaction11"
      reaction_meta_id "_906022"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "a0_tr+a_tr*KM^n/(KM^n+PX^n)"
      kinetic_law_meta_id "_421292"
    ]
  ]
  node [
    id 59
    zlevel -1

    graphics [
      x 588.5732710782828
      y 453.81647768910807
      w 30.0
      h 30.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 0.0
      type "sourcesink"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "Reaction12"
      type "text"
    ]
    oldlabel "transcription of CI"
    sbgn [
      glyphid "glyph18"
      role "SOURCESINK"
    ]
    sbml [
      _sboterm "SBO:0000183"
      reaction_annotation "annotation"
      reaction_id "Reaction12"
      reaction_meta_id "_906042"
      reaction_non_rdf_annotation "XMLNode [childElements size=1, attributes=XML Attributes : 
, characters=null, column=0, isEndElement=false, isEOF=false, isStartElement=true, isText=false, line=0, namespaces=org.sbml.jsbml.xml.XMLNamespaces@6b9, triple=annotation]"
      reversible "false"
      sbmlRole "reaction"
    ]
    sbml_kinetic_law [
      kinetic_law_function "a0_tr+a_tr*KM^n/(KM^n+PY^n)"
      kinetic_law_meta_id "_421329"
    ]
  ]
  node [
    id 61
    zlevel -1

    graphics [
      x 434.678
      y 316.758
      w 810.0
      h 594.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 8.0
      gradient 0.0
      opacity 1.0
      rounding 60.0
      type "rectangle"
    ]
    label "cell"
    labelgraphics [
      alignment "center"
      anchor "itr"
      color "#000000"
      fontName "Arial"
      fontSize 25
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbgn [
      glyphid "glyph1"
      role "COMPARTMENT"
    ]
  ]
  edge [
    id 8
    source 4
    target 7
    SBGN [
      BendIn "153.81678029370306;195.7773396712799"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 153.81678029370306 y 195.7773396712799 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc1"
      role "CONSUMPTION"
    ]
    sbml [
      reactant_meta_id "_420973"
      sbmlRole "reactant"
      species "X"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 10
    source 5
    target 9
    SBGN [
      BendIn "733.5779670501672;176.78687893204506"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 733.5779670501672 y 176.78687893204506 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc2"
      role "CONSUMPTION"
    ]
    sbml [
      reactant_meta_id "_420999"
      sbmlRole "reactant"
      species "Y"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 12
    source 6
    target 11
    SBGN [
      BendIn "434.36674922759846;494.71166002059203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 434.36674922759846 y 494.71166002059203 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc3"
      role "CONSUMPTION"
    ]
    sbml [
      reactant_meta_id "_421025"
      sbmlRole "reactant"
      species "Z"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 14
    source 13
    target 1
    SBGN [
      BendOut "233.59643729979325;93.41741555168863"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 233.59643729979325 y 93.41741555168863 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc4"
      role "PRODUCTION"
    ]
    sbml [
      product_meta_id "_421051"
      sbmlRole "product"
      species "PX"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 15
    source 4
    target 13
    graphics [
      fill "#808080"
      outline "#FF3333"
      arrow "last"
      arrowheadstyle "trigger"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 15.0
    ]
    sbgn [
      glyphid "arc5"
      role "NECESSARYSTIMULATION"
    ]
    sbml [
      _sboterm "SBO:0000461"
      modifier_meta_id "_421064"
      sbmlRole "modifier"
      species "X"
    ]
  ]
  edge [
    id 17
    source 16
    target 2
    SBGN [
      BendOut "649.6974935857017;326.3027136243913"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 649.6974935857017 y 326.3027136243913 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc6"
      role "PRODUCTION"
    ]
    sbml [
      product_meta_id "_421088"
      sbmlRole "product"
      species "PY"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 18
    source 5
    target 16
    graphics [
      fill "#808080"
      outline "#FF3333"
      arrow "last"
      arrowheadstyle "trigger"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 15.0
    ]
    sbgn [
      glyphid "arc7"
      role "NECESSARYSTIMULATION"
    ]
    sbml [
      _sboterm "SBO:0000461"
      modifier_meta_id "_421100"
      sbmlRole "modifier"
      species "Y"
    ]
  ]
  edge [
    id 20
    source 19
    target 3
    SBGN [
      BendOut "308.6754827510962;426.81647768910807"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 308.6754827510962 y 426.81647768910807 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc8"
      role "PRODUCTION"
    ]
    sbml [
      product_meta_id "_421124"
      sbmlRole "product"
      species "PZ"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 21
    source 6
    target 19
    graphics [
      fill "#808080"
      outline "#FF3333"
      arrow "last"
      arrowheadstyle "trigger"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 15.0
    ]
    sbgn [
      glyphid "arc9"
      role "NECESSARYSTIMULATION"
    ]
    sbml [
      _sboterm "SBO:0000461"
      modifier_meta_id "_421136"
      sbmlRole "modifier"
      species "Z"
    ]
  ]
  edge [
    id 23
    source 1
    target 22
    SBGN [
      BendIn "523.5725745828834;93.41741555168863"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 523.5725745828834 y 93.41741555168863 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc10"
      role "CONSUMPTION"
    ]
    sbml [
      reactant_meta_id "_421160"
      sbmlRole "reactant"
      species "PX"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 25
    source 2
    target 24
    SBGN [
      BendIn "547.5725745828834;290.7367185499778"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 547.5725745828834 y 290.7367185499778 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc11"
      role "CONSUMPTION"
    ]
    sbml [
      reactant_meta_id "_421184"
      sbmlRole "reactant"
      species "PY"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 27
    source 3
    target 26
    SBGN [
      BendIn "366.623325130618;325.3027136243913"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 366.623325130618 y 325.3027136243913 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc12"
      role "CONSUMPTION"
    ]
    sbml [
      reactant_meta_id "_421208"
      sbmlRole "reactant"
      species "PZ"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 29
    source 28
    target 4
    SBGN [
      BendOut "209.59643729979325;301.3027136243913"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 209.59643729979325 y 301.3027136243913 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc13"
      role "PRODUCTION"
    ]
    sbml [
      product_meta_id "_421232"
      sbmlRole "product"
      species "X"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 30
    source 3
    target 28
    graphics [
      fill "#808080"
      outline "#FF3333"
      arrow "last"
      arrowheadstyle "inhibitor"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 15.0
    ]
    sbgn [
      glyphid "arc14"
      role "INHIBITION"
    ]
    sbml [
      _sboterm "SBO:0000536"
      modifier_meta_id "_421244"
      sbmlRole "modifier"
      species "PZ"
    ]
  ]
  edge [
    id 32
    source 31
    target 5
    SBGN [
      BendOut "458.36674922759846;176.78687893204506"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 458.36674922759846 y 176.78687893204506 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc15"
      role "PRODUCTION"
    ]
    sbml [
      product_meta_id "_421268"
      sbmlRole "product"
      species "Y"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 33
    source 1
    target 31
    graphics [
      fill "#808080"
      outline "#FF3333"
      arrow "last"
      arrowheadstyle "inhibitor"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 15.0
    ]
    sbgn [
      glyphid "arc16"
      role "INHIBITION"
    ]
    sbml [
      _sboterm "SBO:0000536"
      modifier_meta_id "_421280"
      sbmlRole "modifier"
      species "PX"
    ]
  ]
  edge [
    id 35
    source 34
    target 6
    SBGN [
      BendOut "523.5725745828834;450.81647768910807"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 523.5725745828834 y 450.81647768910807 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc17"
      role "PRODUCTION"
    ]
    sbml [
      product_meta_id "_421304"
      sbmlRole "product"
      species "Z"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 36
    source 2
    target 34
    graphics [
      fill "#808080"
      outline "#FF3333"
      arrow "last"
      arrowheadstyle "inhibitor"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 15.0
    ]
    sbgn [
      glyphid "arc18"
      role "INHIBITION"
    ]
    sbml [
      _sboterm "SBO:0000536"
      modifier_meta_id "_421317"
      sbmlRole "modifier"
      species "PY"
    ]
  ]
  edge [
    id 38
    source 7
    target 37
    SBGN [
      BendOut "105.81678029370306;195.7773396712799"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 105.81678029370306 y 195.7773396712799 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc19"
      role "PRODUCTION"
    ]
  ]
  edge [
    id 40
    source 9
    target 39
    SBGN [
      BendOut "781.5779670501672;176.78687893204506"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 781.5779670501672 y 176.78687893204506 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc20"
      role "PRODUCTION"
    ]
  ]
  edge [
    id 42
    source 11
    target 41
    SBGN [
      BendOut "434.36674922759846;542.711660020592"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 434.36674922759846 y 542.711660020592 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc21"
      role "PRODUCTION"
    ]
  ]
  edge [
    id 44
    source 43
    target 13
    SBGN [
      BendIn "185.59643729979325;93.41741555168863"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 185.59643729979325 y 93.41741555168863 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc22"
      role "CONSUMPTION"
    ]
  ]
  edge [
    id 46
    source 45
    target 16
    SBGN [
      BendIn "697.6974935857017;326.3027136243913"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 697.6974935857017 y 326.3027136243913 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc23"
      role "CONSUMPTION"
    ]
  ]
  edge [
    id 48
    source 47
    target 19
    SBGN [
      BendIn "308.6754827510962;474.81647768910807"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 308.6754827510962 y 474.81647768910807 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc24"
      role "CONSUMPTION"
    ]
  ]
  edge [
    id 50
    source 22
    target 49
    SBGN [
      BendOut "571.5725745828834;93.41741555168863"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 571.5725745828834 y 93.41741555168863 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc25"
      role "PRODUCTION"
    ]
  ]
  edge [
    id 52
    source 24
    target 51
    SBGN [
      BendOut "547.5725745828834;242.7367185499778"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 547.5725745828834 y 242.7367185499778 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc26"
      role "PRODUCTION"
    ]
  ]
  edge [
    id 54
    source 26
    target 53
    SBGN [
      BendOut "414.623325130618;325.3027136243913"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 414.623325130618 y 325.3027136243913 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc27"
      role "PRODUCTION"
    ]
  ]
  edge [
    id 56
    source 55
    target 28
    SBGN [
      BendIn "209.59643729979325;349.3027136243913"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 209.59643729979325 y 349.3027136243913 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc28"
      role "CONSUMPTION"
    ]
  ]
  edge [
    id 58
    source 57
    target 31
    SBGN [
      BendIn "410.36674922759846;176.78687893204506"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 410.36674922759846 y 176.78687893204506 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc29"
      role "CONSUMPTION"
    ]
  ]
  edge [
    id 60
    source 59
    target 34
    SBGN [
      BendIn "571.5725745828834;450.81647768910807"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 571.5725745828834 y 450.81647768910807 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "none"
      frameThickness 1.5
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 15.0
    ]
    sbgn [
      glyphid "arc30"
      role "CONSUMPTION"
    ]
  ]
]
