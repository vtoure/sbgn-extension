package org.sbgnextension;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.jws.soap.SOAPBinding.Style;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.sbgn.SbgnUtil;
import org.sbgn.bindings.Bbox;
import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.Label;
import org.sbgn.bindings.Map;
import org.sbgn.bindings.SBGNBase.Extension;
import org.sbgn.bindings.Sbgn;
import org.sbgnextension.Annotation.SBGNannotation;
import org.sbgnextension.Colors.SBGNcolor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteExtension {
	private static final String renderSBMLuri = "http://projects.eml.org/bcb/sbml/render/level2";
	private static final String annotationuri = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	
	
	public void writeSBGN(File f) throws ParserConfigurationException, JAXBException{
		Sbgn sbgn = new Sbgn();
		Map map = new Map();
		sbgn.setMap(map);
		
		SBGNcolor sbgncolors = Application.getcolors();
		writeColors(map, sbgncolors);
		
		//Create one glyph
		Glyph glyph = new Glyph();
		glyph.setId("glyph7");
		glyph.setClazz("macromolecule");
		Bbox bbox1 = new Bbox();
		bbox1.setX(125);
		bbox1.setY(60);
		bbox1.setW(100);
		bbox1.setH(40);
		glyph.setBbox(bbox1);
		
		Label label1 = new Label();
		label1.setText("LacI mRNA");
		glyph.setLabel(label1);
		
		//Get the annotation for the created glyph, from the input file.
		ArrayList<SBGNannotation> sbgnannot = Application.getannotations();
		for(int i=0; i < sbgnannot.size(); i++){
			if(sbgnannot.get(i).getID().equals(glyph.getId())){
				writeAnnotation(sbgnannot.get(i), glyph);
			}
		}
		map.getGlyph().add(glyph);
		SbgnUtil.writeToFile(sbgn, f);
		
	}
	public void writeColors(Map map, SBGNcolor sbgnc) throws ParserConfigurationException{
		Extension ext = new Extension();
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element elt = doc.createElementNS(renderSBMLuri, "renderInformation");
		
		//Create elements for the list of color definitions
		Element listcolordef = doc.createElement("listOfColorDefinitions");
		for (int i = 0; i < sbgnc.getcolordef().size(); i++) {
			Element colordef = doc.createElement("colorDefinition");
			colordef.setAttribute("id", sbgnc.getcolordef().get(i).getID());
			colordef.setAttribute("value", sbgnc.getcolordef().get(i).getColorDef());
			
			//Add <colorDefinition> to <listOfColorDefinitions>
			listcolordef.appendChild(colordef);
		}
		
		//Create elements for the list of gradient definitions
		Element listgradef = doc.createElement("listOfGradientDefinitions");
		for (int i = 0; i < sbgnc.getgraddef().size(); i++) {
			Element graddef = doc.createElement("linearGradient");
			graddef.setAttribute("x1", "0%");
			graddef.setAttribute("y1", "0%");
			graddef.setAttribute("z1", "0%");
			graddef.setAttribute("x2", "100%");
			graddef.setAttribute("y2", "0%");
			graddef.setAttribute("z2", "100%");
			graddef.setAttribute("id", sbgnc.getgraddef().get(i).getID());
			graddef.setAttribute("spreadMethod", "reflect");
			
			//Create element for gradient color:
			Element stopFrom = doc.createElement("stop");
			stopFrom.setAttribute("offset", "0%");
			stopFrom.setAttribute("stop-color", sbgnc.getgraddef().get(i).getFrom());
			Element stopTo = doc.createElement("stop");
			stopTo.setAttribute("offset", "100%");
			stopTo.setAttribute("stop-color", sbgnc.getgraddef().get(i).getTo());
			
			//Add gradient colors to <linearGradient>
			graddef.appendChild(stopFrom);
			graddef.appendChild(stopTo);
			//Add <linearGradient> to <listOfGradientDefinitions>
			listgradef.appendChild(graddef);
		}
		
		//Create element for the styles applied to the glyphs
		Element liststyle = doc.createElement("listOfStyles");
		for(int i = 0; i < sbgnc.getstyledef().size(); i++){
			Element styledef = doc.createElement("style");
			styledef.setAttribute("idList", sbgnc.getstyledef().get(i).getIDs());
			Element styleinfo = doc.createElement("g");
			styleinfo.setAttribute("stroke", sbgnc.getstyledef().get(i).getStroke());
			styleinfo.setAttribute("stroke-width", sbgnc.getstyledef().get(i).getStrokeWidth());
			if(sbgnc.getstyledef().get(i).getFill() != null){
				styleinfo.setAttribute("fill", sbgnc.getstyledef().get(i).getFill());
			}
			styledef.appendChild(styleinfo);
			liststyle.appendChild(styledef);
		}
		
		
		elt.appendChild(listcolordef);
		elt.appendChild(listgradef);
		elt.appendChild(liststyle);
		ext.getAny().add(elt);
		map.setExtension(ext);
	}
	
	public void writeAnnotation(SBGNannotation annot, Glyph g) throws ParserConfigurationException{
		String id = annot.getID();
		HashMap<String, ArrayList<String>> bioQualCVterm = annot.getBioqual() ;
		
		// add extension data
		Extension ext = new Extension();
			
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element elt = doc.createElementNS(annotationuri, "rdf:RDF");
		elt.setAttribute("xmlns:bqbiol","http://biomodels.net/biology-qualifiers/");
		
		Element description = doc.createElement("rdf:Description");
		description.setAttribute("rdf:about", "#"+id);
		description.setNodeValue(id);
		for (Entry<String, ArrayList<String>> entry : bioQualCVterm.entrySet()) {
			Element bqbiol = doc.createElement(entry.getKey());
			Element bag = doc.createElement("rdf:Bag");
			for(String CvTerm : entry.getValue()){
				Element cvterm = doc.createElement("rdf:li");
				cvterm.setAttribute("rdf:ressource", CvTerm);
				bag.appendChild(cvterm);
			}
			bqbiol.appendChild(bag);
			description.appendChild(bqbiol);
		}



		elt.appendChild(description);
		ext.getAny().add(elt);
		g.setExtension(ext);
	}
}
