package org.sbgnextension;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Map.Entry;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.sbgn.SbgnUtil;
import org.sbgn.bindings.Sbgn;
import org.sbgnextension.Annotation.SBGNannotation;
import org.sbgnextension.Colors.SBGNcolor;

public class Application {
		
	private static SBGNcolor sbgncolors;
	private static ArrayList<SBGNannotation> sbgnannot;
	
	public Application(){
		Application.sbgncolors = new SBGNcolor();
		Application.sbgnannot = new ArrayList<SBGNannotation>();
	}
	
	public static SBGNcolor getcolors(){
		return sbgncolors;
	}
	
	public static ArrayList<SBGNannotation> getannotations(){
		return sbgnannot;
	}

	public static void main(String[] args) throws JAXBException, ParserConfigurationException
	{
		Application app = new Application();
		
		if(args.length == 2){
			//Read the SBGN file given as first input
			ReadExtension read = new ReadExtension();
			File f = new File(args[0]);
			Sbgn sbgn = SbgnUtil.readFromFile(f);
			read.getColors(sbgn, sbgncolors);
			read.getAnnotation(sbgn, sbgnannot);
			
			printColorInfo();
			printAnnotInfo();
			
			//Write the new SBGN file example
			WriteExtension write = new WriteExtension();
			File output = new File(args[1]);
			write.writeSBGN(output);
		}
		else
			System.err.println("Invalid command line, exactly two arguments are required: one input SBGN file and one output SBGN file");
		
		
	}
	
	private static void printColorInfo(){
		System.out.println("##### Color Definition #####");
		for(int i = 0; i < sbgncolors.getcolordef().size(); i++){
				System.out.println("Color ID: \t" + sbgncolors.getcolordef().get(i).getID());
				System.out.println("Hex code: \t" + sbgncolors.getcolordef().get(i).getColorDef());
				System.out.println();
		}
		
		System.out.println("##### Color Gradient #####");
		for(int i = 0; i < sbgncolors.getgraddef().size(); i++){
			System.out.println("Gradient ID: \t" + sbgncolors.getgraddef().get(i).getID());
			System.out.println("From: \t \t" + sbgncolors.getgraddef().get(i).getFrom());
			System.out.println("To: \t \t" + sbgncolors.getgraddef().get(i).getTo()); 
		System.out.println();
		}
		
		System.out.println("##### Styles #####");
		for(int i = 0; i < sbgncolors.getstyledef().size(); i++){
			System.out.println("Target Glyph IDs: \t" + sbgncolors.getstyledef().get(i).getIDs());
			System.out.println("Stroke: \t \t" + sbgncolors.getstyledef().get(i).getStroke());
			System.out.println("Stroke width: \t \t" + sbgncolors.getstyledef().get(i).getStrokeWidth());
			if(sbgncolors.getstyledef().get(i).getFill() == null)
				System.out.println("Fill: \t \t \t-");
			else
			System.out.println("Fill: \t \t  \t" + sbgncolors.getstyledef().get(i).getFill());
			System.out.println();
			
		}
	}

	private static void printAnnotInfo(){
		
		for(int i=0; i < sbgnannot.size(); i++){
			System.out.println("ID: \t \t" + sbgnannot.get(i).getID());
			for (Entry<String, ArrayList<String>> entry : sbgnannot.get(i).getBioqual().entrySet()) {
				System.out.println("BioQual: \t" + entry.getKey());
				for(String CVterm : entry.getValue()){
					System.out.println("CVTerm: \t" + CVterm);
				}
				System.out.println();
			}
				
			System.out.println();
		}
	}
}
