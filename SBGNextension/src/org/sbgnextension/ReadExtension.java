package org.sbgnextension;

import java.util.ArrayList;

import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.Map;
import org.sbgn.bindings.SBGNBase.Extension;
import org.sbgnextension.Annotation.SBGNannotation;
import org.sbgnextension.Colors.ColorDefinition;
import org.sbgnextension.Colors.GradientDefinition;
import org.sbgnextension.Colors.SBGNcolor;
import org.sbgnextension.Colors.StyleDefinition;
import org.sbgn.bindings.Sbgn;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ReadExtension {
	private static final String renderSBMLuri = "http://projects.eml.org/bcb/sbml/render/level2";
	private static final String annotationuri = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";


	public void getColors(Sbgn diagram, SBGNcolor sbgnc){
		Map map = diagram.getMap();
		Extension exMap = map.getExtension();
		// read render info
		if (exMap != null && exMap.getAny().size() > 0) {
			for (Element element : exMap.getAny()) {
				if (element.getNamespaceURI() == renderSBMLuri) { // found render information
					// go through color definitions
					NodeList colors = element.getElementsByTagName("colorDefinition");
					for (int i = 0; i < colors.getLength(); i++) {
						Element current = (Element) colors.item(i);
						ColorDefinition coldef = new ColorDefinition(current.getAttribute("value"), current.getAttribute("id"));
						sbgnc.setcolordef(coldef);
					}

					// go through gradient definitions
					NodeList gradients = element.getElementsByTagName("linearGradient");
					for (int i = 0; i < gradients.getLength(); i++) {
						Element current = (Element) gradients.item(i);
						GradientDefinition grad = new GradientDefinition(current.getAttribute("id"),  
								((Element) current.getChildNodes().item(0)).getAttribute("stop-color"), ((Element) current.getChildNodes().item(1)).getAttribute("stop-color"));
						sbgnc.setgraddef(grad);
					}

					// go through styles
					NodeList styles = element.getElementsByTagName("style");

					for (int i = 0; i < styles.getLength(); i++) {
						StyleDefinition style = new StyleDefinition();
						Element current = (Element) styles.item(i);
						style.setID(current.getAttribute("idList"));
						Element group = (Element) current.getChildNodes().item(0);
						final String fill = group.getAttribute("fill");
						if (!fill.isEmpty())
							style.setFill(fill);
						style.setStroke(group.getAttribute("stroke"));
						style.setStrokeWidth(group.getAttribute("stroke-width"));
						sbgnc.setstyledef(style);
					}
				}
			}
		}					
	}

	public void getAnnotation(Sbgn diagram, ArrayList<SBGNannotation> sbgna)
	{
		Map map = diagram.getMap();
		for(Glyph g : map.getGlyph())
		{
			Extension exGlyph = g.getExtension();
			if (exGlyph != null && exGlyph.getAny().size() > 0) 
			{
				for (Element element : exGlyph.getAny())
				{	
					if (element.getNamespaceURI() == annotationuri)
					{
						NodeList children = element.getChildNodes();
						for (int i = 0; i < children.getLength()-1; i++) {
							SBGNannotation annot = new SBGNannotation();
							Element current = (Element)children.item(i);
							annot.setID(current.getAttribute("rdf:about").replaceAll("#", ""));							
							
							//Get the biological qualifiers used in SBML and the CVTerms
							for(int j = 0; j < current.getChildNodes().getLength()-1; j++){
								Element bioqualifer = (Element) current.getChildNodes().item(j);
								ArrayList<String> Cvterms = new ArrayList<String>();
								Element bag = (Element) bioqualifer.getChildNodes().item(0);
								for(int k = 0; k < bag.getChildNodes().getLength()-1 ; k++){
									Element CVterm = (Element) bag.getChildNodes().item(k);
									Cvterms.add(CVterm.getAttribute("rdf:resource").toString());
								}
								annot.setBioqual(bioqualifer.getNodeName(), Cvterms);
							}
							
							sbgna.add(annot);
						}
					}
				}
			}
		}
	}
}
