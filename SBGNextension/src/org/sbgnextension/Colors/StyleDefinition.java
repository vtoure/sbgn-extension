package org.sbgnextension.Colors;


public class StyleDefinition {

	private String styleGlyphIDs;
	private String stroke;
	private String strokewidth;
	private String fill;
	
	public void setID(String id) {
		this.styleGlyphIDs = id;
	}

	public String getIDs() {
			return this.styleGlyphIDs;
	}
	
	public void setStrokeWidth(String strokew){
		this.strokewidth = strokew;
	}
	
	public String getStrokeWidth(){
		return this.strokewidth;
	}
	
	public void setStroke(String stroke){
		this.stroke = stroke;
	}
	
	public String getStroke(){
		return this.stroke;
	}
	
	public void setFill(String fill){
		this.fill = fill;
	}
	
	public String getFill(){
		return this.fill;
	}
}
