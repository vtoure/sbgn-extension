package org.sbgnextension.Colors;

public class ColorDefinition extends SBGNcolor{

	private String colordef;
	private String colorID;
	
	
	public ColorDefinition(String color, String id) {
		this.colordef = color;
		this.colorID = id;
	}
	
	public void setID(String id) {
		this.colorID = id;
		
	}

	public String getID() {
		return this.colorID;
	}

	
	public void setColorDef(String color){
		this.colordef = color;
	}
	
	public String getColorDef(){
		return this.colordef;
	}
	


}
