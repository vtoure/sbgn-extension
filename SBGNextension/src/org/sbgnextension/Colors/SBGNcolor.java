package org.sbgnextension.Colors;

import java.util.ArrayList;

public class SBGNcolor {
	private ArrayList<ColorDefinition> colordef;
	private ArrayList<GradientDefinition> gradientdef;
	private ArrayList<StyleDefinition> styledef;
	
	
	public SBGNcolor() {
		colordef = new ArrayList<ColorDefinition>();
		gradientdef = new ArrayList<GradientDefinition>();
		styledef = new ArrayList<StyleDefinition>();
	}

	
	public void setcolordef(ColorDefinition coldef){
		colordef.add(coldef);
	}
	
	public void setgraddef(GradientDefinition graddef){
		gradientdef.add(graddef);
	}
	
	public void setstyledef(StyleDefinition styldef){
		styledef.add(styldef);
	}
	
	
	public ArrayList<ColorDefinition> getcolordef(){
		return this.colordef;
	}
	
	public ArrayList<GradientDefinition> getgraddef(){
		return this.gradientdef;
	}
	
	public ArrayList<StyleDefinition> getstyledef(){
		return this.styledef;
	}
}
