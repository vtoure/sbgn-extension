package org.sbgnextension.Colors;

public class GradientDefinition extends SBGNcolor{

	private String gradientdefID;
	private String gradientFROM;
	private String gradientTO;
	
	public GradientDefinition(String id, String from, String to) {
		this.gradientdefID = id;
		this.gradientFROM = from;
		this.gradientTO = to;
	}

	public void setID(String id) {
		this.gradientdefID = id;
		
	}

	public String getID() {
		return this.gradientdefID;
	}
	
	public void setFrom(String from){
		this.gradientFROM = from;
	}
	
	public void setTo(String to){
		this.gradientTO = to;
	}
	
	public String getFrom(){
		return gradientFROM;
	}
	
	public String getTo(){
		return gradientTO;
	}
}
