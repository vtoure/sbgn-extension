package org.sbgnextension.Annotation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SBGNannotation {

	private String glyphID;
	private HashMap<String, ArrayList<String>> bioqual_CVterms;
	
	public SBGNannotation(){
		this.bioqual_CVterms = new HashMap<String, ArrayList<String>>();
	}
	
	public void setID(String id){
		this.glyphID = id;
	}
	
	public void setBioqual(String qual, ArrayList<String> CVterms){
		this.bioqual_CVterms.put(qual, CVterms);
	}

	public String getID(){
		return this.glyphID;
	}
	
	public HashMap<String, ArrayList<String>> getBioqual(){
		return this.bioqual_CVterms;
	}
	
}
